# Curriculum Web Service


Observa��o: Projeto testado em cluster Kubernetes executando a vers�o 1.14.3.

Este projeto cria um container de nginx com o arquivo em html do CV Online e configura um ingress p�blico na borda do cluster, cujo tr�fego de entrada ser� enviado ao servi�o que atende as chamadas ao dominio olucasfagundes.otacondev.ninja.


Para executar o projeto:


## Instalar Cert Manager no Cluster
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.12.0/cert-manager.yaml

## Aplicar manifestos para deployment
kubectl apply -f k8s